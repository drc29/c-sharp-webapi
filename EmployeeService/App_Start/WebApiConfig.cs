﻿using System.Web.Http;


namespace EmployeeService
{
    public static class WebApiConfig
    {
        //public class CustomJsonFormatter : JsonMediaTypeFormatter
        //{
        //    public CustomJsonFormatter()
        //    {
        //        this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        //    }

        //    public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        //    {
        //        base.SetDefaultContentHeaders(type, headers, mediaType);
        //        headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    }
        //}

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Remove the XML format, return only JSON from ASP.NET Web API Service
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Remove the Json format, return only XML from ASP.NET Web API Service
            //config.Formatters.Remove(config.Formatters.JsonFormatter);

            // Return JSON instead of XML from ASP.NET Web API Service when a request is made from the browser
            //Approach 1
            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
            //Approache 2
            //config.Formatters.Add(new CustomJsonFormatter());

            // Indent JSON data
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;

            // Camel case instead of Pascal Case
            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver =new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
        }
    }
}
